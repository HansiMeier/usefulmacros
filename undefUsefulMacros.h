
/**************************************************************************************************
 **************************************************************************************************
 **												  VISIBILITY CONTROL													 **
 **************************************************************************************************
 **************************************************************************************************/

#ifdef PUBLIC_API
    #undef PUBLIC_API
#endif

#ifdef PRIVATE_API
    #undef PRIVATE_API
#endif


/**************************************************************************************************
 **************************************************************************************************
 **									  GNU FUNCTION AND VARIABLE ATTRIBUTES										 **
 **************************************************************************************************
 **************************************************************************************************/
#ifdef PURE
    #undef PURE
#endif
#ifdef PURE_NO_GLOBAL
    #undef PURE_NO_GLOBAL
#endif
#ifdef RUN_BEFORE_MAIN
    #undef RUN_BEFORE_MAIN
#endif
#ifdef RUN_AFTER_MAIN
    #undef RUN_AFTER_MAIN
#endif
#ifdef MAYBE_UNUSED
    #undef MAYBE_UNUSED
#endif
#ifdef WARN_IF_RESULT_UNUSED
    #undef WARN_IF_RESULT_UNUSED
#endif
#ifdef DOESNT_THROW
    #undef DOESNT_THROW
#endif
#ifdef RESULT_NEVER_NULLPTR
    #undef RESULT_NEVER_NULLPTR
#endif
#ifdef DOESNT_RETURN
    #undef DOESNT_RETURN
#endif
#ifdef ARGUMENTS_NEVER_NULLPTRS
    #undef ARGUMENTS_NEVER_NULLPTRS
#endif
#ifdef USED_FREQUENTLY
    #undef USED_FREQUENTLY
#endif
#ifdef DEPRECATED
    #undef DEPRECATED
#endif
#ifdef SELDOM_USED
    #undef SELDOM_USED
#endif
#ifdef DESTROY_VARIABLE_WITH
    #undef DESTROY_VARIABLE_WITH
#endif
