#pragma once

/**************************************************************************************************
 **************************************************************************************************
 **                              WHICH COMPILER COMPILES THIS, ETC                               **
 **************************************************************************************************
 **************************************************************************************************/

/*
See
https://sourceforge.net/p/predef/wiki/Home
*/

/**************************************************************************************************
 **************************************************************************************************
 **                                      BASIC MACRO STUFF                                       **
 **************************************************************************************************
 **************************************************************************************************/

#define STRINGIFY(thing) #thing

/**************************************************************************************************
 **************************************************************************************************
 **												  VISIBILITY CONTROL													 **
 **************************************************************************************************
 **************************************************************************************************/

/* A function or variable declared with PUBLIC_API
 * is meant for use by everybody. On the other hand, if
 * it's declared with PRIVATE_API, it's only used internally by this library.
 */
#if defined WIN32 || defined __CYGWIN__
    #if defined BUILDING_DLL
        #define PUBLIC_API __declspec(dllexport)
    #else
        #define PUBLIC_API __declspec(dllimport)
    #endif
    #define PRIVATE_API
#else
    #define PUBLIC_API __attribute__((visibility("default")))
    #define PRIVATE_API __attribute__((visibility("hidden")))
#endif


/**************************************************************************************************
 **************************************************************************************************
 **									  GNU VARIABLE AND FUNCTION ATTRIBUTES						 **
 **************************************************************************************************
 **************************************************************************************************/

#ifdef __GNUC__
    #define PURE __attribute__((__pure__))
    #define PURE_NO_GLOBAL_MEMORY __attribute__((__const__))
    #define RUN_BEFORE_MAIN __attribute__((__constructor__))
    #define RUN_AFTER_MAIN __attribute__((__destructor__))
    #define MAYBE_UNUSED __attribute__((__unused__))
    #define WARN_IF_RESULT_UNUSED __attribute__((__warn_unused_result__))
    #define DOESNT_THROW __attribute__((__northrow__))
    #define RESULT_NEVER_NULLPTR __attribute__((__returns_nonull__))
    #define RETURNS_MORE_THAN_ONCE __attribute((__returns_twice__))
    #define DOESNT_RETURN __attribute__((__noreturn__))
    #define ALL_ARGUMENTS_NEVER_NULLPTRS __attribute__((__nonnull__))
    #define SOME_ARGUMENTS_NEVER_NULLPTRS(...) __attribute__((__nonnull__, ##__VA_ARGS__))
    #define USED_FREQUENTLY __attribute__((__hot__))
    #define DEPRECATED __attribute__((__deprecated__))
    #define SELDOM_USED __attribute__((__cold__))
    #define DESTROY_VARIABLE_WITH(function) __attribute__((__cleanup__(function)))
#else
    #define PURE 
    #define PURE_NO_GLOBAL_MEMORY
    #define RUN_BEFORE_MAIN 
    #define RUN_AFTER_MAIN 
    #define MAYBE_UNUSED 
    #define WARN_IF_RESULT_UNUSED 
    #define DOESNT_THROW 
    #define RESULT_NEVER_NULLPTR
    #define RETURNS_MORE_THAN_ONCE
    #define DOESNT_RETURN 
    #define ALL_ARGUMENTS_NEVER_NULLPTRS
    #define SOME_ARGUMENTS_NEVER_NULLPTRS(...)
    #define USED_FREQUENTLY 
    #define DEPRECATED 
    #define SELDOM_USED 
    #define DESTROY_VARIABLE_WITH(function)
#endif


/**************************************************************************************************
 **************************************************************************************************
 **                        ALTERNATIVES FOR #ERROR, ETC. TO USE IN MACROS                        **
 **************************************************************************************************
 **************************************************************************************************/

#if defined __GNUC__
    #define DO_PRAGMA(p) _Pragma(#p)
    #define COMPILATION_ERROR(msg) DO_PRAGMA(GCC error msg)
    #define COMPILATION_WARNING(msg) DO_PRAGMA(GCC warning msg)
#endif

/**************************************************************************************************
 **************************************************************************************************
 **                                       FORCED INLINING                                        **
 **************************************************************************************************
 **************************************************************************************************/

#if defined __GNUC__
    #define FORCE_INLINE __inline__ __attribute__((__always_inline__, __artificial__))
#elif defined _MSC_VER
    #define FORCE_INLINE __forceinline __inline
#else
    #define FORCE_INLINE COMPILATION_ERROR("FORCE_INLINE not available for this compiler!")
#endif

/**************************************************************************************************
 **************************************************************************************************
 **                               POINTER RESTRICT SUPPORT FOR C++                               **
 **************************************************************************************************
 **************************************************************************************************/

#if defined __cplusplus
    #if defined __GNUC__
        #define restrict __restrict__
    #elif defined _MSC_VER
        #define restrict __restrict
    #endif
#endif

/**************************************************************************************************
 **************************************************************************************************
 **                                        VERSION CHECK                                         **
 **************************************************************************************************
 **************************************************************************************************/

#ifdef __cplusplus
    #define REQUIRE_C_11 COMPILATION_ERROR("This is C code, not C++ code!")
    #define REQUIRE_C_99 COMPILATION_ERROR("This is C code, not C++ code!")

    #if __cplusplus == 201402L
        #define CPP_VERSION 14
        #define REQUIRE_CPP_14
        #define REQUIRE_CPP_11
    #elif __cplusplus == 201103L
        #define CPP_VERSION 11
        #define REQUIRE_CPP_14 COMPILATION_ERROR("This code needs C++14, not C++11!")
        #define REQUIRE_CPP_11
    #else
        #define CPP_VERSION 98
        #define REQUIRE_CPP_14 COMPILATION_ERROR("This code needs C++14, not C++98!")
        #define REQUIRE_CPP_11 COMPILATION_ERROR("This code needs C++11, not C++98!")
    #endif
#else
    #define REQUIRE_CPP_14 COMPILATION_ERROR("This is C++ code, not C code!")
    #define REQUIRE_CPP_11 COMPILATION_ERROR("This is C++ code, not C code!")

    #if defined __STDC_VERSION__
        #if __STDC_VERSION__ == 201112L
            #define C_VERSION 11
            #define REQUIRE_C_11
            #define REQUIRE_C_99
        #elif __STDC_VERSION__ == 199901L
            #define C_VERSION 99
            #define REQUIRE_C_11 COMPILATION_ERROR("This code needs C11, not C99!")
            #define REQUIRE_C_99
        #endif
    #elif defined __STDC__
        #define C_VERSION 89
        #define REQUIRE_C_11 COMPILATION_ERROR("This code needs C11, not C89!")
        #define REQUIRE_C_99 COMPILATION_ERROR("This code needs C99, not C89!")
    #else
        #error "Unknown C version!"
    #endif
#endif

/**************************************************************************************************
 **************************************************************************************************
 **                                       DEBUGGING MACROS                                       **
 **************************************************************************************************
 **************************************************************************************************/

#ifdef __GNUC__
#define NEVER_USE(identifier) DO_PRAGMA(GCC poison #identifier)
#else
#define NEVER_USE(identifier)
#endif

#ifdef NDEBUG
#define STUB COMPILATION_ERROR("Expand me, this is just a stub!")
#else
#define STUB
#endif

/**************************************************************************************************
 **************************************************************************************************
 **                                            OTHER                                             **
 **************************************************************************************************
 **************************************************************************************************/

#define IGNORED_RETURN_VALUE (void)
